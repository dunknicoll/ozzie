var canvas = document.createElement("canvas");
canvas.width = 800;
canvas.height = 600;
var ctx = canvas.getContext("2d");

var world;

var timer;

var width = canvas.width;
var height = canvas.height;

var ozzieReady = false;
var ozzieImage = new Image();

var SCALE = 30;

var ozzie = {
	speed: 5
};

var keysDown = {};

var   b2Vec2 = Box2D.Common.Math.b2Vec2
        , b2BodyDef = Box2D.Dynamics.b2BodyDef
        , b2Body = Box2D.Dynamics.b2Body
        , b2FixtureDef = Box2D.Dynamics.b2FixtureDef
        , b2Fixture = Box2D.Dynamics.b2Fixture
        , b2World = Box2D.Dynamics.b2World
        , b2MassData = Box2D.Collision.Shapes.b2MassData
        , b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape
        , b2CircleShape = Box2D.Collision.Shapes.b2CircleShape
        , b2Shape = Box2D.Collision.Shapes.b2Shape
        , b2DebugDraw = Box2D.Dynamics.b2DebugDraw
          ;

var init = function()
{

	document.body.appendChild(canvas);
     
       world = new b2World(
             new b2Vec2(0, 10)    //gravity
          ,  true                 //allow sleep
       );

       var fixDef = new b2FixtureDef;
       fixDef.density = 1.0;
       fixDef.friction = 0.5;
       fixDef.restitution = 0.2;
     
       var bodyDef = new b2BodyDef;
     
       //create ground
       bodyDef.type = b2Body.b2_staticBody;
       
       // positions the center of the object (not upper left!)
       bodyDef.position.x = canvas.width / 2 / SCALE;
       bodyDef.position.y = canvas.height / SCALE;
       
       fixDef.shape = new b2PolygonShape;
       
       // half width, half height. eg actual height here is 1 unit
       fixDef.shape.SetAsBox(canvas.width / SCALE, 60 / SCALE);
       world.CreateBody(bodyDef).CreateFixture(fixDef); 

       var debugDraw = new b2DebugDraw();
   debugDraw.SetSprite(ctx);
   debugDraw.SetDrawScale(SCALE);
   debugDraw.SetFillAlpha(0.3);
   debugDraw.SetLineThickness(1.0);
   debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
   world.SetDebugDraw(debugDraw);

	ozzieImage.onload = function () {
		ozzieReady = true;
	};

	ozzieImage.src = "images/ozzie.png";

	addEventListener("keydown", function (e) {
		keysDown[e.keyCode] = true;
	}, false);

	addEventListener("keyup", function (e) {
		delete keysDown[e.keyCode];
	}, false);

	ozzie.x = canvas.width / 2;
	ozzie.y = canvas.height / 2;

	bodyDef.type = b2Body.b2_dynamicBody;
	for(var i = 0; i < 10; ++i) {
		fixDef.shape = new b2PolygonShape;
		fixDef.shape.SetAsBox(25.5 / SCALE, 40 / SCALE);
		bodyDef.position.x = Math.random() * (canvas.width / SCALE);
		bodyDef.position.y = Math.random() * ( 10 / SCALE );
		var data = {
			imgsrc: "images/balloon.png",
			iwidth: 51,
			iheight: 80,
			bodysize: 100
		}
		bodyDef.userData = data;
		world.CreateBody(bodyDef).CreateFixture(fixDef);
	}

	main();

};

var update = function()
{

	var elapsed = window.performance.now() - timer;
	step = elapsed / 16.666666667;
	
	timer = window.performance.now();

	ozzie.speed = (5 * step)

	if (38 in keysDown) { // Player holding up
		ozzie.y -= ozzie.speed;
	}
	if (40 in keysDown) { // Player holding down
		ozzie.y += ozzie.speed;
	}
	if (37 in keysDown) { // Player holding left
		ozzie.x -= ozzie.speed;
	}
	if (39 in keysDown) { // Player holding right
		ozzie.x += ozzie.speed;
	}

	 world.Step(
             1 / 60   //frame-rate
          ,  10       //velocity iterations
          ,  10       //position iterations
       );

	
      
};

var render = function()
{

	world.DrawDebugData();
	world.ClearForces();

	if (ozzieReady) {
		ctx.drawImage(ozzieImage, ozzie.x, ozzie.y);
	}
};

var main = function()
{
	clear();
	update();
	render();
	processObjects();
	window.requestAnimationFrame(main);
};

var clear = function()
{
	ctx.save();
	ctx.fillStyle = "rgb(153,153,51)";
	ctx.fillRect(0,0,width,height);
	ctx.restore();
}

function processObjects() {
		 var node = world.GetBodyList();
		 while (node) {
			var b = node;
			node = node.GetNext();
			// Destroy objects that have floated off the screen
			var position = b.GetPosition();
				// Canvas Y coordinates start at opposite location, so we flip
				var fl = b.GetFixtureList();
				if (!fl) {
					continue;
				}
				// put an image in place if we store it in user data
				if (b.m_userData && b.m_userData.imgsrc) {
					// Draw the image on the object
					var iwidth = b.m_userData.iwidth / SCALE;
					var iheight = b.m_userData.iheight / SCALE;
					var imgObj = new Image(iwidth,iheight);

					imgObj.src = b.m_userData.imgsrc;
					ctx.save();
					// Translate to the center of the object, then flip and scale appropriately
					ctx.translate(position.x * SCALE,position.y * SCALE); 
					ctx.rotate(b.GetAngle());
					var w2 = 0-((iwidth*SCALE)/2);
					var h2 = 0-((iheight*SCALE)/2);

					ctx.drawImage(imgObj,w2,h2);
					ctx.restore();
					//b.ApplyImpulse(new b2Vec2(6000,6000),new b2Vec2(0,0));
					//b.ApplyImpulse(new b2Vec2(6000,6000),b.GetWorldCenter());
					 

			 }
		 }
	 }

init();